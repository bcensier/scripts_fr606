#!/usr/bin/env python
# -*- coding: utf-8 -*-

#       Copyright © 2018 Benjamin Censier  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
#       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.

import argparse
import sys
import os
import struct
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pdb

def read_bst_lofar(filename, verbose=False):
    # Read LOFAR beamlet statistics binary file

    
    RSPboard = int(filename.split('_')[-1][:2])

    if verbose:
        print "\n> beamlet file from RSP board number "+str(RSPboard)

    date_temp = filename.split('/')[-1].split('_')[0]
    time_temp = filename.split('/')[-1].split('_')[1]
    date_obs = datetime.datetime(year = int(date_temp[:4]), month = int(date_temp[4:6]),day = int(date_temp[6:8]),hour = int(time_temp[:2]), minute = int(time_temp[2:4]), second = int(time_temp[-2:]))

    if verbose:
        print "\n> File time stamp is: "+str(date_obs)
    
    Nbeamlets = 488 # per file... don't get that at all but seems to work (BC 08/2016)

    
    Npol = 2
    stat = os.stat(filename)
    Nbytes = stat.st_size # size of file in octets
    Nbytes = Nbytes
    Nt = Nbytes/8/Nbeamlets # total time length derived from size in octets

    if verbose:
        print "\n>"+str(Nt)+" time samples found."
    

    fid = open(filename)
    timeline = []

    for ntime_index in range(Nt):

        data = np.array(struct.unpack("<"+Nbeamlets*"d",fid.read(Nbeamlets*8))) # "<" for little endian
        timeline.append(data)

    return np.array(timeline)

# function for indices parsing
def _parse_slice(s):
        a = [int(e) if e.strip() else None for e in s.split(":")]
        if len(a) == 1:
            return slice(a[0],a[0]+1,1)
        else:
            return slice(*a)


########## Parse command line arguments #########################

parser = argparse.ArgumentParser(description='Plot beamlet statistics profile from lofar bst files.')

parser.add_argument("-f", "--file", type=str, default="", help="Lofar bst file to be plotted")
parser.add_argument("-t", "--time_index", type=_parse_slice, default=":", help="Set of time indices to be plotted. Python conventions: index_begin:index_end:index_step")
parser.add_argument("-b", "--beamlets", type=_parse_slice, default=":", help="Set of beamlet indices to be plotted. Python conventions: index_begin:index_end:index_step")
parser.add_argument("-l", "--log", action="store_true",help="Plot log version normalized to max")

parser.add_argument("-v", "--verbose", action = "store_true",help="Spit out the logs")

args = parser.parse_args()
filename = args.file
time_index = args.time_index
beamlets = args.beamlets
log = args.log
verbose = args.verbose


if verbose:
    print "\n>> Verbose ON <<"
    print "######## Input parameters ########"
    print "> Reading file: "+filename
    print "> time_indices: "+str(time_index.start)+" to "+str(time_index.stop)+", step = "+str(time_index.step)
    print "> beamlets indices:  "+str(beamlets.start)+" to "+str(beamlets.stop)+", step = "+str(beamlets.step)
    print "################################## \n"


timeline = read_bst_lofar(filename,verbose=verbose)



data = timeline[time_index,beamlets]

if log:
    data = 10*np.log10(data/np.max(data.flatten()))

    
if len(data.shape) == 1:
    twoD = False
elif len(data.shape) == 2:
    if data.shape[0] ==1 or data.shape[1] == 1:
        twoD = False
    else:
        twoD = True

print str(data.shape)+" gggg"

if not twoD: # 1D case


    pdb.set_trace()    

    if time_index.start == time_index.stop-1:
        plt.plot(data.T)
        plt.xlabel('Subband index in the file')
        plot_title = "\nOne time slot vs. subbands. Time index : "+str(time_index.start)

    else:
        plt.plot(data)
        plt.xlabel('Time index')
        plot_title = "\nOne subband vs. time. Subband index in the file : "+str(beamlets.start)
    if log:
        plottitle = filename+"\ndB scale (norm to max)"+plot_title
        plt.ylabel("Power in the beam (dB, norm to max)")
    else:
        plottitle = filename+"\nlinear scale"+plot_title
        plt.ylabel("Power in the beam (linear, a.u.)")
        
else: # 2D case time/frequency
    plt.imshow(data,interpolation="None")
    plt.gca().set_aspect('auto')
    plt.colorbar()
    if log:
        plottitle = filename+"\ndB scale (norm to max)"
    else:
        plottitle = filename+"\nlinear scale"
    plt.xlabel("Subband index in the file")
    plt.ylabel("Time index")

plt.title(plottitle,usetex=False)
plt.show()
